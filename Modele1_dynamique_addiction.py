# -*- coding: utf-8 -*-
"""
@author: Albane CHALLAMEL,Erwan SABAROTS,Léa SADZA
"""

import matplotlib.pyplot as plt



#Définition des constantes 
d=0.2 #effet d'oubli 
q=0.8 # quantité max de ce que peut ingérer un humain en 1 semaine >=0
Sm=0.5 #self controle maximum
b=2*d/q # l'influence du passage à l'acte dans le désir de passer à l'acte
p=0.2   #0.2-0.8 -> résilience psychologique
h=p*Sm # coeff de la compétition direct entre S et C
k=(p/q)*Sm # coefficient de la résultante directe du passage à l'acte
mE=-0.06 # dégradation uniforme de l’influence sociale 



#Définition des conditions initiales

C0=1 # intensité de fringale ou désir / varie entre 0 et 1 
S0 = Sm #intensité de self contrôle 
E0=-1 #correspond à l'intensité de l'influence sociale / varie entre -1 et 1 



def projet(tmax) :
#PARAMETRES :
    #ENTREE :
        
        #tmax un entier correspondant au nombre de semaine sur lesquels on étudie le comportement d'un individu
    #SORTIE :
        
        #listeV :une liste contenant les valeurs correspondant au niveau de vulnérabilité à chaque instant t
        #listeC :une liste contenant les valeurs correspondant à l'intensité du désir à chaque instant t
        #listeS : une liste contenant les valeurs correspondant à l'intensité du self contrôle à chaque instant t
        #listeA :une liste contenant les valeurs correspondant au niveau du passage à l'acte à chaque instant t 
        #listeE :une liste contenant les valeurs correspondant à l'influence sociale à chaque instant t 
        
    # Initialisation des listes pour stocker les valeurs de chaque variable à chaque itération
    listeE = [E0]
    listeS = [S0]
    listeC = [C0]
    listeA = []
    listePSI = []
    listeV=[]
    
    # Boucle qui permet de simuler l'évolution du modèle à chaque instant t
    for t in range(tmax) :

        e = listeE[t] - mE 
        listeE.append(e)

        psi = listeC[t] - listeS[t] - listeE[t]
        listePSI.append(psi)

        v = min(1, max (listePSI[t],0))
        listeV.append(v)

        a = q * listeV[t]
        listeA.append(a)

        gamma = b * min(1, 1-listeC[t])
        c = listeC[t] - d * listeC[t] + gamma * listeA[t]
        listeC.append(c)
        
        #Sans amélioration 
        s = listeS[t] + p * max(0,Sm - listeS[t]) - h * listeC[t] - k * listeA[t]
        #Avec amélioration (S >= 0)
        #s = max(0,listeS[t] + p * max(0,Sm - listeS[t]) - h * listeC[t] - k * listeA[t])
        listeS.append(s)

    return(listeV,listeC,listeS,listeA,listeE)

#On définit le temps maximal sur lequel on étudie (en semaines)
tmax=52
listeV,listeC,listeS,listeA,listeE=projet(tmax)

'''
#On fait varier C0 de 0.1 à 1 et on trace les différents résultats sur un même graphe en fonction du temps
for k in range(1,11):
    C0=k/10
   listeV,listeC,listeS,listeA,listeE=projet(tmax)
    plt.title("Représentation graphique de l'intensité de fringale ou de désir par rapport au temps (C) \n avec S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
    plt.plot(listeC,label="C0="+str(k/10))
    plt.xlabel("temps(semaines) ")
    plt.ylabel("Intensité de fringale ou de désir")
    plt.legend(loc='lower right ')
plt.show()


#On fait varier S0 de 0.1 à 1 et on trace les différents résultats sur un même graphe en fonction du temps 
for k in range(1,11):
    S0=k/10
    listeV,listeC,listeS,listeA,listeE=projet(tmax)
    plt.title("Représentation graphique de l'intensité de self-contrôle par rapport au temps (S) \n avec C0="+str(C0)+"; p ="+str(p) +"; E=" +str(E0))
    plt.plot(listeS,label="S0="+str(k/10))
    plt.xlabel("temps (semaines)")
    plt.ylabel("Intensité de self-controle")
    plt.legend(loc='lower right')
plt.show()
'''

#On trace l'évolution de l'intensité du self-controle(S), du niveau de vulnérabilité(V) et de l'influence sociale(E) sur un même graphe, le tout en fonction du temps
plt.title("Représentation graphique avec C0="+str(C0)+" ;S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
plt.plot(listeS,label="Intensité de self-controle (S)")
plt.plot(listeV,label="Niveau de vulnérabilité (V)")
plt.plot(listeE,label="Influence sociale (E)")
plt.xlabel("temps (semaines)")
plt.legend()
plt.show()


#On trace l'évolution du niveau de passage à l'acte et de l'intensité de fringale sur un même graphe, le tout en fonction du temps

plt.title("Représentation graphique avec C0="+str(C0)+" ;S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
plt.plot(listeC,label="Intensité de fringale ou de désir (C)")
plt.plot(listeA,label="Passage à l'acte (A)")
plt.xlabel("temps (semaines)")
plt.legend()
plt.show()


#On trace l'évolution de l'influence sociale en fonction du temps
plt.title("Représentation graphique avec C0="+str(C0)+" ;S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
plt.plot(listeE,label="Influence sociale (E)")
plt.xlabel("temps (semaines)")
plt.legend()
plt.show()




'''
#On trace chaque attribut sur un graphe différents en fonction du temps

plt.title("Représentation graphique de l'intensité de self-contrôle par rapport au temps (S) \n avec C0="+str(C0)+" ;S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
plt.plot(listeS)
plt.xlabel("temps (semaines)")
plt.ylabel("Intensité de self-controle (S)")
plt.legend()
plt.show()

plt.title("Représentation graphique du niveau de vulnérabilité par rapport au temps (V) \n avec C0="+str(C0)+" ;S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
plt.plot(listeV)
plt.xlabel("temps (semaines)")
plt.ylabel("Niveau de vulnérabilité (V)")
plt.legend()
plt.show()


plt.title("Représentation graphique de l'intensité de fringale ou de désir par rapport au temps (C) \n avec C0="+str(C0)+" ;S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
plt.plot(listeC)
plt.xlabel("temps(semaines) ")
plt.ylabel("Intensité de fringale ou de désir (C)")
plt.legend()
plt.show()

plt.title("Représentation graphique du passage à l'acte par rapport au temps (A) \n avec C0="+str(C0)+" ;S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
plt.plot(listeA)
plt.xlabel("temps (semaines)")
plt.ylabel("Passage à l'acte (A)")
plt.legend()
plt.show()



#On trace tout les attribus sur un même graphe en fonction du temps
    
plt.title("Représentation graphique avec C0="+str(C0)+" ;S0="+str(S0)+"; p ="+str(p) +"; E=" +str(E0))
plt.plot(listeS,label="Intensité de self-controle (S)")
plt.plot(listeV,label="Niveau de vulnérabilité (V)")
plt.plot(listeC,label="Intensité de fringale ou de désir (C)")
plt.plot(listeA,label="Passage à l'acte (A)")
plt.xlabel("temps (semaines)")
plt.legend()
plt.show()

'''