# -*- coding: utf-8 -*-
"""
@author: Albane CHALLAMEL,Erwan SABAROTS,Léa SADZA
"""

import numpy as np
import matplotlib.pyplot as plt




#définition des constantes 
d=0.2 #effet d'oubli 
q=0.8 # quantité max de ce que peut ingérer un humain en 1 semaine >=0
Sm=0.5 #self controle maximum
b=2*d/q # l'influence du passage à l'acte dans le désir de passer à l'acte
p0=0.4 #0.2 à 0.8 #0.2-0.8 -> résilience psychologique
h=0.25 #p*Sm  # coeff de la compétition direct entre S et C
k=0.5 #(p/q)*Sm # coefficient de la résultante directe du passage à l'acte
mE=-0.06 # mE >0 => dégradation / mE<0 => amélioration # incrémentation uniforme de l’influence sociale 
mlambda=0.01 # incrémentation uniforme des occasions sociales 
Rm=7

#définition des conditions initiales
C0=0.1 # intensité de fringale / varie entre 0 et 1 
S0 = Sm  #intensité de self contrôle 
E0=-1 #correspond à l'intensité de l'influence sociale / varie entre -1 et 1 
lambda0=0.02 #correspond à la fréquence des occasions sociales / varie entre 0 et 1 



def projet2(tmax):
#PARAMETRES :
    #ENTREE :
        
        #tmax un entier correspondant au nombre de semaine sur lesquels on étudie le comportement d'un individu
    #SORTIE :
        
        #listeV :une liste contenant les valeurs correspondant au niveau de vulnérabilité à chaque instant t
        #listeC :une liste contenant les valeurs correspondant à l'intensité du désir à chaque instant t
        #listeS : une liste contenant les valeurs correspondant à l'intensité du self contrôle à chaque instant t
        #listeA :une liste contenant les valeurs correspondant au niveau du passage à l'acte à chaque instant t 
        #OccasSocial :une liste contenant les valeurs correspondant aux occasions sociales sociale à chaque instant t 
        #listeE :une liste contenant les valeurs correspondant à l'influence sociale à chaque instant t 
        #sigma :un réel entre 0 et 1 correspondant au temps de dépendance par rapport au temps maximal  
        
    # Initialisation des listes pour stocker les valeurs de chaque variable à chaque itération
    listeE = [E0]
    listeS = [S0]
    listeC = [C0]
    listeLambda = [lambda0]
    listeA = []
    listePSI = []
    listeV=[]
    listeR = []
    OccasSocial=[]
    
    # durée de dependance
    tdep = 0 
    
    # Boucle qui permet de simuler l'évolution du modèle à chaque instant t
    for t in range(tmax) :
        p=p0*np.sqrt(t/tmax) #permet d'obtenir un p non constant qui évolue en fonction du t 
        
        #permet de diminuer l'influence sociale toutes les 3 semaines 
        if(t%3==0):
            e=listeE[t] - mE
        else:
            e=listeE[t]
        listeE.append(e)

        psi = listeC[t] - listeS[t] - listeE[t]
        listePSI.append(psi)

        v = min(1, max (listePSI[t],0))
        listeV.append(v)
        
        if (listeV[t]>=0.8) : #On incrémente le temps de dépendance que si le niveau de vulnabilité au temps t est au dessus de 0.8
            tdep = tdep + 1 
        
        # cas ou Lambda non constant 
        Lambda=lambda0*np.sqrt(t/tmax)
        
        # cas ou Lambda constant 
        #Lambda = listeLambda[t] + mlambda 
        
        listeLambda.append(Lambda)
        
        #On utilise une loi de poisson afin de générer aléatoirement des valeurs 
        r = np.random.poisson(listeLambda[t])
        listeR.append(r)
        
        occas=q * (1-listeV[t]) * listeR[t]/Rm
        OccasSocial.append(occas)
        
        a = q * listeV[t] + OccasSocial[t]
        listeA.append(a)

        gamma = b * min(1, 1-listeC[t])
        c = listeC[t] - d * listeC[t] + gamma * listeA[t]
        listeC.append(c)

        #sans amélioration 
        s = listeS[t] + p * max(0,Sm - listeS[t]) - h * listeC[t] - k * listeA[t]
        
        #avec amélioration ( S>=0 )
        #s = max(0,listeS[t] + p * max(0,Sm - listeS[t]) - h * listeC[t] - k * listeA[t])

        listeS.append(s)
        
    sigma = tdep/tmax 

        
    return(listeV,listeC,listeS,listeA,OccasSocial,listeE,sigma)

#On définit le temps maximal sur lequel on étudie (en semaines)
tmax=100
listeV,listeC,listeS,listeA,OccasSocial,listeE,sigma=projet2(tmax)

'''
#On initialise les différentes liste que l'on va incrémenter par la suite

Scoring=[]
Ecarttype=[]
Somme=[]
#nbsimu le nombre de personnes que l'on étudie 
nbsimu=50

# permet d'étudier sur un échantillon de personnes l'éfficacité d'une thérapie
# lors de l'utilisation de la loi de poisson, chaque run correspond à l'étude d'un individu, on étudie ici la dynamique du comportement de nbsimu individus

#ETUDE DU SCORING ET DE L'ECART TYPE (p)

for i in range(1,11):
    p0=i/10 #On fait varier P0 de 0.1 à 1
    listeSigma =  [] #initialisation de la liste qui va stocker les sigmas
    for pi in range(nbsimu) :
        listeV,listeC,listeS,listeA,OccasSocial,listeE,sigma=projet2(tmax)
        listeSigma.append(sigma)
    scoring = np.mean(listeSigma)# le scoring correspons à la tendance de l'effet de p sur l'addiction
    Scoring.append(scoring)
    ecarttype=np.sqrt(np.var(listeSigma))
    Ecarttype.append(ecarttype)
    somme=Scoring[i-1]+Ecarttype[i-1]
    Somme.append(somme)

#On affiche la somme de l'écart type et de la moyenne en fonction de p 

listeP=np.arange(0.1,1.1,0.1)
plt.title("Représentation graphique de la somme de l'écart type et de la moyenne \n par rapport à p avec "+str(nbsimu)+" simulations")
plt.plot(listeP,Somme)
plt.grid(True)
plt.xlabel("p")
plt.ylabel("Scoring (p) + $\sigma$(p)")
plt.legend()
plt.show()

#On affiche le scoring en fonction de p 

listeP=np.arange(0.1,1.1,0.1)
plt.title("Représentation graphique du scoring par rapport à p avec "+str(nbsimu)+" simulations")
plt.plot(listeP,Scoring)
plt.xlabel("p")
plt.ylabel("Scoring (p) ")
plt.grid(True)
plt.legend()
plt.show()


    

#ETUDE DU SCORING ET DE L'ECART TYPE (lambda)

for i in range(1,51):
    lambda0=i/10  #On fait lambda0 P0 de 0.1 à 5
    listeSigma =  [] #initialisation de la liste qui va stocker les sigmas
    for pi in range(nbsimu) :
        listeV,listeC,listeS,listeA,OccasSocial,listeE,sigma=projet2(tmax)
        listeSigma.append(sigma)
    scoring = np.mean(listeSigma) # le scoring correspons à la tendance de l'effet de p sur l'addiction
    Scoring.append(scoring)
    ecarttype=np.sqrt(np.var(listeSigma))
    Ecarttype.append(ecarttype)
    somme=Scoring[i-1]+Ecarttype[i-1]
    Somme.append(somme)
    

#On affiche la somme de l'écart type et de la moyenne en fonction de lambda

ListeLambda=np.arange(0.1,5.1,0.1)
plt.title("Représentation graphique de la somme de l'écart type et de la moyenne \npar rapport à $\lambda$ avec "+str(nbsimu)+" simulations")
plt.plot(ListeLambda,Somme)
plt.grid(True)
plt.xlabel("$\lambda$")
plt.ylabel("Scoring ($\lambda$) + $\sigma$($\lambda$)")
plt.legend()
plt.show()

#On affiche le scoring en fonction de lambda

ListeLambda=np.arange(0.1,5.1,0.1)
plt.title("Représentation graphique du scoring par rapport à $\lambda$ avec "+str(nbsimu)+" simulations")
plt.plot(ListeLambda,Scoring)
plt.xlabel("$\lambda$")
plt.ylabel("Scoring ($\lambda$) ")
plt.grid(True)
plt.legend()
plt.show()




#On trace l'évolution du niveau de passage à l'acte et de l'intensité de fringale sur un même graphe, le tout en fonction du temps


plt.title("Représentation graphique avec C0="+str(C0)+" ;S0="+str(S0)+"; mE ="+str(mE) +"; E0=" +str(E0))
plt.plot(listeC,label="Intensité de fringale ou de désir (C)")
plt.plot(listeA,label="Passage à l'acte (A)")
plt.xlabel("temps (semaines)")
plt.legend()

#On trace l'évolution de l'intensité du self-controle(S), du niveau de vulnérabilité(V) et de l'influence sociale(E) sur un même graphe, le tout en fonction du temps

plt.title("Représentation graphique avec C0="+str(C0)+" ;S0="+str(S0)+"; mE ="+str(mE) +"; E0=" +str(E0))
plt.plot(listeS,label="Intensité de self-controle (S)")
plt.plot(listeV,label="Niveau de vulnérabilité (V)")
plt.plot(listeE,label="Influences sociales (E)")
plt.xlabel("temps (semaines)")
plt.legend()
plt.show()

#On trace l'évolution de l'influence sociale et des occasions sociales sur un même graphe, le tout en fonction du temps

plt.title("Représentation graphique avec C0="+str(C0)+" ;S0="+str(S0)+"; mE ="+str(mE) +"; E0=" +str(E0))
plt.plot(listeE,label="Influence sociale (E)")
plt.plot(OccasSocial,label="Occasions sociales (OccasSocial)")
plt.xlabel("temps (semaines)")
plt.legend()
plt.show()

#On trace tout les attribus sur un même graphe en fonction du temps

plt.title("Représentation graphique avec C0="+str(C0)+" ;S0="+str(S0)+"; mlambda ="+str(mlambda) +"\n p="+str(p))
plt.plot(listeS,label="Intensité de self-controle (S)")
plt.plot(listeV,label="Niveau de vulnérabilité (V)")
plt.plot(listeC,label="Intensité de fringale ou de désir (C)")
plt.plot(listeA,label="Passage à l'acte (A)")
plt.plot(listeE,label="Influences sociales (E)")
plt.plot(OccasSocial,label="Occasions sociales (OccasSocial)")
plt.xlabel("temps (semaines)")
plt.legend(fontsize=8,loc='lower left')
plt.show()
'''